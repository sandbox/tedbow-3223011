<?php

namespace Drupal\update_plus\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

abstract class StagerFormBase extends FormBase {

  /**
   * @var \Drupal\update_plus\Updater
   */
  protected $updater;

  protected function getExistingStagedOperationWarning(): array {
    if ($this->updater->hasActiveUpdate()) {
      $form['existing']['#markup'] = $this->t(
        'There is an existing Composer operation. You must <a href="@url" >complete or delete this operation first.</a>',
        ['@url' => Url::fromRoute('update_plus.confirmation_page')->toString()]
      );
      return $form;
    }
    return [];
  }

}