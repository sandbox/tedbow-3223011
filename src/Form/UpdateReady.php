<?php

namespace Drupal\update_plus\Form;

use Drupal\update_plus\Updater;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Commit staged updates.
 *
 * @internal
 */
class UpdateReady extends FormBase {

  /**
   * The updater service.
   *
   * @var \Drupal\update_plus\Updater
   */
  protected $updater;

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;


  /**
   * Constructs a new UpdateReady object.
   *
   * @param \Drupal\update_plus\Updater $updater
   *   The updater service.
   */
  public function __construct(Updater $updater, MessengerInterface $messenger, StateInterface $state) {
    $this->updater = $updater;
    $this->setMessenger($messenger);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'update_plus_update_ready_form';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('update_plus.updater'),
      $container->get('messenger'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!$this->updater->getActiveStagerKey()) {
      $this->messenger->addError($this->t('No active update'));
      return $this->redirect('update_plus.update_form');
    }
    $error_messages = $this->updater->validateStaged();
    if ($error_messages) {
      foreach ($error_messages as $error_message) {
        $this->messenger->addError($error_message);
        $form['delete'] = [
          '#type' => 'submit',
          '#value' => $this->t('Delete current operation'),
          '#name' => 'delete'
        ];
      }
      return $form;
    }
    $form['backup'] = [
      '#prefix' => '<strong>',
      '#markup' => $this->t('Back up your database and site before you continue. <a href=":backup_url">Learn how</a>.', [':backup_url' => 'https://www.drupal.org/node/22281']),
      '#suffix' => '</strong>',
    ];

    $form['maintenance_mode'] = [
      '#title' => $this->t('Perform updates with site in maintenance mode (strongly recommended)'),
      '#type' => 'checkbox',
      '#default_value' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Continue'),
      '#name' => 'commit'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $submitted_button = $form_state->getTriggeringElement()['#name'];
    if ($submitted_button === 'delete') {
      $this->updater->clean();
      return;
    }
    $session = $this->getRequest()->getSession();
    // Store maintenance_mode setting so we can restore it when done.
    $session->set('maintenance_mode', $this->state->get('system.maintenance_mode'));
    if ($form_state->getValue('maintenance_mode') == TRUE) {
      $this->state->set('system.maintenance_mode', TRUE);
      // @todo unset after updater. After db update?
    }

    // @todo Should these operations be done in batch.
    $this->updater->commit();
    // Clean could be done in another page load or on cron to reduce page time.
    $this->updater->clean();
    $this->messenger->addMessage("Update complete!");

    // @todo redirect to update.php?
    $form_state->setRedirect('update_plus.update_form');
  }

}
